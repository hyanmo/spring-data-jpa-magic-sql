package cn.wewin.modules.springdata.jpa.hibernate.dialect;

import org.hibernate.dialect.MySQL8Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;

/**
 *
 * @author jession
 * @date 7/25/16
 */
public class MySqlDialect extends MySQL8Dialect {
    public MySqlDialect() {
        super();
        //THIS WORKS!
        registerFunction("bitwise_and", new SQLFunctionTemplate(StandardBasicTypes.INTEGER, "(?1 & ?2)"));
        registerFunction("date_add_interval",
                new SQLFunctionTemplate(StandardBasicTypes.DATE, "DATE_ADD(?1, INTERVAL ?2 ?3)"));
        registerFunction("convert", new SQLFunctionTemplate(StandardBasicTypes.STRING, "convert(?1 using ?2)") );
    }
}
