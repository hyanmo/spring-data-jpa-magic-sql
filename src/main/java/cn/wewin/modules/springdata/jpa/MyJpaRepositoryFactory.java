package cn.wewin.modules.springdata.jpa;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * JPA Repository工厂
 * User: Jession
 * Date: 13-5-23
 * Time: 下午2:26
 * @author jession
 */
public class MyJpaRepositoryFactory extends JpaRepositoryFactory {

    public MyJpaRepositoryFactory(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected JpaRepositoryImplementation<?, ?> getTargetRepository(RepositoryInformation information, EntityManager entityManager) {
        JpaEntityInformation<?, Serializable> entityInformation = this.getEntityInformation(information.getDomainType());

        SimpleJpaRepository<?, ?> repo =
                isCustomDynamicQuery(information.getRepositoryInterface())
                        ? new MySimpleJpaRepository(entityInformation, entityManager, information.getRepositoryInterface())
                        : getTargetRepositoryViaReflection(information, entityInformation, entityManager);

        return repo;
    }

    @Override
    protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
        if (isCustomDynamicQuery(metadata.getRepositoryInterface())) {
            return MySimpleJpaRepository.class;
        } else {
            return SimpleJpaRepository.class;
        }
    }

    private boolean isCustomDynamicQuery(Class<?> repositoryInterface) {
        return MyJpaRepository.class.isAssignableFrom(repositoryInterface);
    }
}
