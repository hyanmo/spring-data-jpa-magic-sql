package cn.wewin.modules.springdata.jpa.hibernate.dialect;

import org.hibernate.type.StandardBasicTypes;

import java.sql.Types;

/**
 * @author Jession
 *
 */
public class SqlServer2008Dialect extends org.hibernate.dialect.SQLServer2008Dialect {
	public SqlServer2008Dialect() {
		super();
		registerColumnType(Types.NVARCHAR, "nvarchar($l)" );
		registerColumnType(Types.NCHAR, "nchar(1)" );
        registerColumnType(Types.LONGNVARCHAR, "nvarchar(max)");

        registerHibernateType(Types.NVARCHAR, StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.NCHAR, StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.LONGNVARCHAR, StandardBasicTypes.TEXT.getName());
	}
}
