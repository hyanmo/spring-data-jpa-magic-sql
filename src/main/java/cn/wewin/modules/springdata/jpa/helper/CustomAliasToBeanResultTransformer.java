package cn.wewin.modules.springdata.jpa.helper;

import org.hibernate.HibernateException;
import org.hibernate.property.access.internal.PropertyAccessStrategyBasicImpl;
import org.hibernate.property.access.internal.PropertyAccessStrategyChainedImpl;
import org.hibernate.property.access.internal.PropertyAccessStrategyFieldImpl;
import org.hibernate.property.access.internal.PropertyAccessStrategyMapImpl;
import org.hibernate.property.access.spi.Setter;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.type.TimestampType;
import org.hibernate.type.descriptor.java.*;
import org.hibernate.type.descriptor.sql.TimestampTypeDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.*;

/**
 * Result transformer that allows to transform a result to
 * a user specified class which will be populated via setter
 * methods or fields matching the alias names.
 * <p/>
 * <pre>
 * List resultWithAliasedBean = s.createCriteria(Enrolment.class)
 * 			.createAlias("student", "st")
 * 			.createAlias("course", "co")
 * 			.setProjection( Projections.projectionList()
 * 					.add( Projections.property("co.description"), "courseDescription" )
 * 			)
 * 			.setResultTransformer( new CustomAliasToBeanResultTransformer(StudentDTO.class) )
 * 			.list();
 *
 *  StudentDTO dto = (StudentDTO)resultWithAliasedBean.get(0);
 * 	</pre>
 *
 * Created with IntelliJ IDEA.
 * User: Jession
 * Date: 13-5-23
 * Time: 下午2:26
 * @author jession
 */
public class CustomAliasToBeanResultTransformer implements ResultTransformer, Serializable {

    private static final long serialVersionUID = -6923958377579709784L;
    // IMPL NOTE : due to the delayed population of setters (setters cached
    // 		for performance), we really cannot properly define equality for
    // 		this transformer
    /**
     * LOG
     */
    protected final Logger log = LoggerFactory.getLogger(getClass());
    private final Class resultClass;
    private boolean isInitialized;
    private String[] aliases;
    private Setter[] setters;
    private JavaTypeDescriptor[] types;

    private static final Map<Class, JavaTypeDescriptor> TYPES_CONVERT_MAP = new HashMap<Class, JavaTypeDescriptor>();

    static {
        TYPES_CONVERT_MAP.put(Character.class, new CharacterTypeDescriptor());
        TYPES_CONVERT_MAP.put(char.class, new CharacterTypeDescriptor());
        TYPES_CONVERT_MAP.put(String.class, new StringTypeDescriptor());
        TYPES_CONVERT_MAP.put(Byte.class, new ByteTypeDescriptor());
        TYPES_CONVERT_MAP.put(byte.class, new ByteTypeDescriptor());
        TYPES_CONVERT_MAP.put(Short.class, new ShortTypeDescriptor());
        TYPES_CONVERT_MAP.put(short.class, new ShortTypeDescriptor());
        TYPES_CONVERT_MAP.put(Integer.class, new IntegerTypeDescriptor());
        TYPES_CONVERT_MAP.put(int.class, new IntegerTypeDescriptor());
        TYPES_CONVERT_MAP.put(Long.class, new LongTypeDescriptor());
        TYPES_CONVERT_MAP.put(long.class, new LongTypeDescriptor());
        TYPES_CONVERT_MAP.put(BigInteger.class, new BigIntegerTypeDescriptor());
        TYPES_CONVERT_MAP.put(BigDecimal.class, new BigDecimalTypeDescriptor());
        TYPES_CONVERT_MAP.put(Float.class, new FloatTypeDescriptor());
        TYPES_CONVERT_MAP.put(float.class, new FloatTypeDescriptor());
        TYPES_CONVERT_MAP.put(Double.class, new DoubleTypeDescriptor());
        TYPES_CONVERT_MAP.put(double.class, new DoubleTypeDescriptor());
        TYPES_CONVERT_MAP.put(Boolean.class, new BooleanTypeDescriptor());
        TYPES_CONVERT_MAP.put(boolean.class, new BooleanTypeDescriptor());
        TYPES_CONVERT_MAP.put(byte[].class, new ByteArrayTypeDescriptor());
        TYPES_CONVERT_MAP.put(Byte[].class, new ByteArrayTypeDescriptor());
        TYPES_CONVERT_MAP.put(Date.class, new DateTypeDescriptor());
        TYPES_CONVERT_MAP.put(Timestamp.class, new JdbcTimestampTypeDescriptor());
    }

    public CustomAliasToBeanResultTransformer(Class resultClass) {
        if (resultClass == null) {
            throw new IllegalArgumentException("resultClass cannot be null");
        }
        isInitialized = false;
        this.resultClass = resultClass;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object transformTuple(Object[] tuple, String[] aliases) {
        Object result = null;

        try {
            if (!isInitialized) {
                initialize(aliases);
            } else {
                check(aliases);
            }

            result = resultClass.getDeclaredConstructor().newInstance();

            for (int i = 0; i < aliases.length; i++) {
                if (setters[i] != null) {
                    setters[i].set(result, types[i].wrap(tuple[i], null), null);
                }
            }
        } catch (InstantiationException e) {
            throw new HibernateException("Could not instantiate resultclass: " + resultClass.getName());
        } catch (IllegalAccessException e) {
            throw new HibernateException("Could not instantiate resultclass: " + resultClass.getName());
        } catch (NoSuchMethodException e) {
            throw new HibernateException("Could not instantiate resultclass: " + resultClass.getName());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return result;
    }

    private void initialize(String[] aliases) {
        PropertyAccessStrategyChainedImpl propertyAccessStrategy = new PropertyAccessStrategyChainedImpl(
                PropertyAccessStrategyBasicImpl.INSTANCE,
                PropertyAccessStrategyFieldImpl.INSTANCE,
                PropertyAccessStrategyMapImpl.INSTANCE
        );

        this.aliases = new String[aliases.length];
        setters = new Setter[aliases.length];
        types = new JavaTypeDescriptor[aliases.length];
        for (int i = 0; i < aliases.length; i++) {
//			String alias = aliases[ i ];
            String alias = convertColumnToProperty(aliases[i]);
            if (alias != null) {
                this.aliases[i] = alias;
                try {
                    setters[i] = propertyAccessStrategy.buildPropertyAccess(resultClass, alias).getSetter();
                    types[i] = getJavaType(Reflections.getAccessibleField(resultClass, alias));
                } catch (Exception e) {
                    log.warn(e.getMessage());
                    setters[i] = null;
                }
                if (types[i] == null) {
                    setters[i] = null;
                }
            }
        }
        isInitialized = true;
    }

    private JavaTypeDescriptor getJavaType(Field accessibleField) {
        return TYPES_CONVERT_MAP.get(accessibleField.getType());
    }

    private void check(String[] aliases) {
        String[] checkAliases = new String[aliases.length];

        for (int i = 0; i < aliases.length; i++) {
            checkAliases[i] = convertColumnToProperty(aliases[i]);
        }
        if (!Arrays.equals(checkAliases, this.aliases)) {
            throw new IllegalStateException(
                    "aliases are different from what is cached; aliases=" + Arrays.asList(aliases) +
                            " cached=" + Arrays.asList(this.aliases));
        }
    }

    /**
     * Converts the specified 'XXX_YYY_ZZZ'-like column name to its
     * 'xxxYyyZzz'-like Java property name.
     *
     * @param columnName the column name
     * @return the Java property name
     */
    private String convertColumnToProperty(String columnName) {
        columnName = columnName.toLowerCase();

        StringBuffer buff = new StringBuffer(columnName.length());
        StringTokenizer st = new StringTokenizer(columnName, "_");

        while (st.hasMoreTokens()) {
            buff.append(StringUtils.capitalize(st.nextToken()));
        }

        buff.setCharAt(0, Character.toLowerCase(buff.charAt(0)));

        return buff.toString();
    }

    @Override
    public List transformList(List collection) {
        return collection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomAliasToBeanResultTransformer that = (CustomAliasToBeanResultTransformer) o;

        if (!resultClass.equals(that.resultClass)) {
            return false;
        }
        return Arrays.equals(aliases, that.aliases);
    }

    @Override
    public int hashCode() {
        int result = resultClass.hashCode();
        result = 31 * result + (aliases != null ? Arrays.hashCode(aliases) : 0);
        return result;
    }
}
