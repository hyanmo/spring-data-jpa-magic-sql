package cn.wewin.modules.springdata.jpa.hibernate;

import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author jession
 */
public class SearchFilter {

    public String fieldName;
    public Object value;
    public Operator operator;

    public SearchFilter(String fieldName, Operator operator, Object value) {
        this.fieldName = fieldName;
        this.value = value;
        this.operator = operator;
    }

    /**
     * searchParams中key的格式为OPERATOR_FIELDNAME
     */
    public static List<SearchFilter> parseMap(Map<String, Object> searchParams) {
        List<SearchFilter> filters = new ArrayList<>(searchParams.size());

        for (Entry<String, Object> entry : searchParams.entrySet()) {
            // 过滤掉空值
            String key = entry.getKey();
            Object value = entry.getValue();
            if (StringUtils.isBlank((String) value)) {
                continue;
            }

            // 拆分operator与filedAttribute
            String[] names = StringUtils.split(key, "_");
            if (names.length != 2) {
                continue;
            }
            String filedName = names[1];
            Operator operator = Operator.valueOf(names[0]);

            // 创建searchFilter
            SearchFilter filter = new SearchFilter(filedName, operator, value);
            filters.add(filter);
        }

        return filters;
    }

    /**
     * searchParams中key的格式为OPERATOR_FIELDNAME
     */
    public static Map<String, Object> toQueryMap(Map<String, Object> searchParams) {
        Map<String, Object> filterMap = new HashMap<>(searchParams.size());

        for (Entry<String, Object> entry : searchParams.entrySet()) {
            // 过滤掉空值
            String key = entry.getKey();
            Object value = entry.getValue();

            if (value instanceof String) {
                String strvalue = String.valueOf(entry.getValue()).trim();
                if (StringUtils.isBlank(strvalue)) {
                    continue;
                }

                value = strvalue;
            }

            String filedName;
            Operator operator;

            // 拆分operator与filedAttribute
            String[] names = StringUtils.split(key, "_");
            if (names.length != 2) {
                throw new IllegalArgumentException(key + " is not a valid search filter name");
            }
            filedName = names[1];
            operator = Operator.valueOf(names[0]);
            switch (operator) {
                case LIKE:
                    value = "%" + value + "%";
                    break;
                case EQ:
                    break;
                default:
                    break;
            }
            filterMap.put(filedName, value);
        }

        return filterMap;
    }

    public String getKey() {
        return operator + "_" + fieldName;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SearchFilter)) {
            return false;
        }
        SearchFilter that = (SearchFilter) o;
        return Objects.equals(fieldName, that.fieldName) &&
                Objects.equals(getValue(), that.getValue()) &&
                operator == that.operator;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldName, getValue(), operator);
    }

    /**
     * Define an enumeration called Operator, with 6 elements, referred to as:
     * Operator.EQ, Operator.LIKE, Operator.GT, Operator.GT, Operator.GTE, Operator.LTE.
     */
    public enum Operator {
        /**
         * Equals
         */
        EQ,
        /**
         * Like
         */
        LIKE,
        /**
         * Great than
         */
        GT,
        /**
         * Less than
         */
        LT,
        /**
         * Great than equals
         */
        GTE,
        /**
         * Less than equals
         */
        LTE
    }
}
