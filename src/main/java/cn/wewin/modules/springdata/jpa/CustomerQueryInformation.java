package cn.wewin.modules.springdata.jpa;

import org.hibernate.Session;

/**
 *
 * @author jession
 * @date 6/27/16
 */
public class CustomerQueryInformation {
    private String query;
    private String countQuery;
    private Session session;

    CustomerQueryInformation(Session session, String query, String countQuery) {
        this.query = query;
        this.countQuery = countQuery;
        this.session = session;
    }


    public Session getSession() {
        return session;
    }

    public String getQuery() {
        return query;
    }

    public String getCountQuery() {
        return this.countQuery;
    }
}