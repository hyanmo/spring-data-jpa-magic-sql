/**
 *  概要  ：上海巍运信息科技有限公司Framework
 *  作者  ：纪三军
 *  备注  ：作成。
 *
 *  历史：
 *  日期  　　　      担当者名　　　　　　　　　　备注
 *  -----------------------------------------------------
 *  2008-9-10    纪三军          　         作成
 *
 *
 *  ALL RIGHTS RESERVED,COPYRIGHT(C) WE-WIN LIMITED 2007
 */
package cn.wewin.modules.springdata.jpa.hibernate;

import cn.wewin.modules.springdata.jpa.CustomerQueryInformation;
import cn.wewin.modules.springdata.jpa.helper.Reflections;
import cn.wewin.modules.springdata.jpa.helper.SqlBuilder;
import cn.wewin.modules.springdata.jpa.helper.TextTemplates;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.Session;
import org.hibernate.hql.internal.ast.QueryTranslatorImpl;
import org.hibernate.hql.spi.QueryTranslator;
import org.hibernate.hql.spi.QueryTranslatorFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.param.NamedParameterSpecification;
import org.hibernate.param.ParameterSpecification;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jession
 */
public final class JpaHqlQueries<T, ID> {

    public static final String SQL_ORDER_BY = "order by";
    public static final String SQL_GROUP_BY = "group by";
    public static final String SQL_COUNT_APPEND_PREFIX = "select count(1) from (";
    public static final String SQL_COUNT_APPEND_ALIAS = ") countalias";
    public static final String SQL_FROM = "from";
    public static final String SQL_COUNT_PREFIX = "select count(*) from ";
    public static final String SQL_UNION_ = "union ";
    private Session session;
    private CustomerQueryInformation queryInformation;
    private Class<T> entityClass;

    @SuppressWarnings("unchecked")
    public JpaHqlQueries(CustomerQueryInformation queryInformation) {
        this.session = queryInformation.getSession();
        this.queryInformation = queryInformation;
        this.entityClass = Reflections.getSuperClassGenricType(getClass(), 0);
    }

    /**
     * 根据页面参数返回分页查询的结果List。
     *
     * @param inParams 分页信息
     * @return 分页查询的结果List
     */
    public List<T> findAll(final Map<String, Object> inParams) {
        return findPage(inParams, null).getContent();
    }

    /**
     * 根据页面参数返回分页查询的结果List。
     *
     * @param inParams    SQL参数
     * @param pageable 分页信息
     * @return 分页查询的结果List
     */
    @SuppressWarnings("unchecked")
    public Page<T> findPage(final Map<String, Object> inParams, final Pageable pageable) {
        Query<T> query = createQuery(queryInformation.getQuery(), inParams);
        return pageable == null ? new PageImpl(query.list()) : readPage(query, inParams, pageable);
    }

    /**
     * 根据页面参数返回分页查询的结果List。
     *
     * @param inParams 分页信息
     * @return 分页查询的结果List
     */
    public List<?> findObjects(final Map<String, Object> inParams) {
        return findPage(inParams, null).getContent();
    }

    /**
     * 根据页面参数返回分页查询的结果List。
     *
     * @param inParams    SQL参数
     * @param pageable 分页信息
     * @return 分页查询的结果List
     */
    @SuppressWarnings("unchecked")
    public Page<?> findObjects(final Map<String, Object> inParams, final Pageable pageable) {
        Query query = createQuery(queryInformation.getQuery(), inParams);
        return pageable == null ? new PageImpl(query.list()) : readPage(query, inParams, pageable);
    }

    /**
     * Reads the given {@link javax.persistence.TypedQuery} into a {@link org.springframework.data.domain.Page} applying the given {@link org.springframework.data.domain.Pageable} and
     * {@link org.springframework.data.jpa.domain.Specification}.
     *
     * @param query            must not be {@literal null}.
     * @param inParamsForTotal can be {@literal null}.
     * @param pageable         can be {@literal null}.
     * @return
     */
    private Page<T> readPage(Query<T> query, Map<String, Object> inParamsForTotal, Pageable pageable) {

        query.setFirstResult((int)pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        return PageableExecutionUtils.getPage(query.list(), pageable, () -> countHqlResult(inParamsForTotal));
    }

    /**
     * 按SQL查询唯一对象.
     *
     * @param values 命名参数,按名称绑定.
     */
    public T findOneEntity(final Map<String, Object> values) {
        return findOneEntity(queryInformation.getQuery(), values);
    }

    /**
     * 按SQL查询唯一对象.
     *
     * @param values 命名参数,按名称绑定.
     */
    private T findOneEntity(final String queryString, final Map<String, Object> values) {
        return createQuery(queryString, values).uniqueResult();
    }

    /**
     * 按SQL查询唯一对象.
     *
     * @param values 命名参数,按名称绑定.
     */
    public Object findOneObject(final Map<String, Object> values) {
        return findOneObject(queryInformation.getQuery() , values);
    }

    /**
     * 按SQL查询唯一对象.
     *
     * @param values 命名参数,按名称绑定.
     */
    private Object findOneObject(final String queryString, final Map<String, Object> values) {
        return createQuery(queryString, values).uniqueResult();
    }

    /**
     * 执行count查询获得本次Sql查询所能获得的对象总数.
     * <p/>
     * 本函数只能自动处理简单的hql语句,复杂的hql查询请另行编写count语句查询.
     */
    @SuppressWarnings("unchecked")
    public long countHqlResult(final Map<String, Object> inParams) {
        String countQuery = queryInformation.getCountQuery();
        String query = queryInformation.getQuery();
        boolean isCountQuery = StringUtils.isNotBlank(countQuery);

        String finalSql = isCountQuery ? countQuery : query;

        List<String> paramList = TextTemplates.fixParamForCompileSql(finalSql, inParams);
        Map fixedParams = new LinkedHashMap<String, Object>(paramList.size());
        for (String p : paramList) {
            Object value = inParams.get(p);
            if (value instanceof String) {
                if (StringUtils.isNotBlank((String)value)) {
                    fixedParams.put(p, inParams.get(p));
                }
            } else if (value != null) {
                fixedParams.put(p, inParams.get(p));
            }
        }

        String countHql = SqlBuilder.parse(finalSql, inParams);

        // if provide count hql, direct return result;
        if (isCountQuery) {
            try {
                return ((Number) findOneObject(countHql, fixedParams)).longValue();
            } catch (Exception e) {
                throw new RuntimeException("hql can't be auto count, hql is:" + countHql, e);
            }
        }

        countHql = transHql(countHql);
        countHql = StringUtils.substringBeforeLast(countHql, SQL_ORDER_BY);

        if (StringUtils.indexOfIgnoreCase(finalSql, SQL_GROUP_BY) >= 0 || StringUtils.indexOfIgnoreCase(finalSql, SQL_UNION_) >= 0) {
            countHql = SQL_COUNT_APPEND_PREFIX + countHql + SQL_COUNT_APPEND_ALIAS;

        } else {
            countHql = StringUtils.substringAfter(countHql, SQL_FROM);
            countHql = SQL_COUNT_PREFIX + countHql;
        }

        try {
            return ((Number) createNativeSqlQuery(countHql, fixedParams, paramList).uniqueResult()).longValue();

        } catch (Exception e) {
            throw new RuntimeException("hql can't be auto count, hql is:" + countHql, e);
        }
    }

    /**
     * 根据查询HQL与参数列表创建Query对象.
     * 与find()函数可进行更加灵活的操作.
     *
     * @param values 命名参数,按名称绑定.
     */
    private NativeQuery createNativeSqlQuery(final String queryString, final Map<String, Object> values, final List<String> paramNames) {
        Assert.hasText(queryString, "queryString不能为空");
        NativeQuery query = session.createNativeQuery(SqlBuilder.parse(queryString, values));
        if (values != null) {
            query.setProperties(values);
        }
        return query;
    }

    public String transHql(String hql) {
        SessionFactoryImpl sfi = (SessionFactoryImpl) session.getSessionFactory();
        QueryTranslatorFactory qtf = sfi.getServiceRegistry().getService(QueryTranslatorFactory.class);
        QueryTranslator qt = qtf.createQueryTranslator("", hql, java.util.Collections.EMPTY_MAP, sfi, null);
        qt.compile(java.util.Collections.EMPTY_MAP, false);

        String sql = qt.getSQLString();

        List<ParameterSpecification> ps = ((QueryTranslatorImpl) qt).getCollectedParameterSpecifications();
        List<String> queryPNames = new ArrayList<>(ps.size());
        ps.forEach(s -> queryPNames.add(((NamedParameterSpecification)s).getName()));
        sql = TextTemplates.repeatReplace(sql, "?", queryPNames);

        return sql;
    }


    /**
     * 根据查询HQL与参数列表创建Query对象.
     * 与find()函数可进行更加灵活的操作.
     *
     * @param values 命名参数,按名称绑定.
     */
    private Query<T> createQuery(final String queryString, final Map<String, Object> values) {
        Assert.hasText(queryString, "queryString不能为空");
        Query<T> query = session.createQuery(SqlBuilder.parse(queryString, values), entityClass);
        if (values != null) {
            query.setProperties(values);
        }
        return query;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(-987654321, 37, this);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return <code>true</code> if this object is the same as the obj argument;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * Retrieves a text representation for this object.
     *
     * @return the text representation
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
