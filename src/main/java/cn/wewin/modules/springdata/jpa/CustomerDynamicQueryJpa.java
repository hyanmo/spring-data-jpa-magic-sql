package cn.wewin.modules.springdata.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Jession
 * @date 14-08-05
 */
public interface CustomerDynamicQueryJpa<T> {

    /**
     * 执行count查询获得本次Sql查询所能获得的对象总数.
     * <p/>
     * 本函数只能自动处理简单的hql语句,复杂的hql查询请另行编写count语句查询.
     *
     * @param resourceKey 查询语句
     * @param inParams 查询参数
     * @return 结果数
     */
    long countHqlResult(final String resourceKey, final Map<String, Object> inParams);

    /**
     * 根据查询语句Key和查询参数返回clazz对象的List。
     *
     * @param resourceKey          指定的SQL语句ID
     * @param inParams     SQL参数
     * @param mappingClass Mapping Class名
     * @return 结果DTO List
     */
    <E> List<E> findAllByNativeQuery(final String resourceKey, Map<String, Object> inParams, Class<E> mappingClass);

    /**
     * 根据查询语句Key和查询参数返回clazz对象的List。
     *
     * @param resourceKey          指定的SQL语句ID
     * @param inParams     SQL参数
     * @return 结果Map List
     */
    List<Map<String, Object>> findAllByNativeQuery(final String resourceKey, Map<String, Object> inParams);

    /**
     * 根据查询语句Key和查询参数返回clazz对象的List。
     *
     * @param resourceKey          指定的SQL
     * @param inParams     SQL参数
     * @param pageable  分页信息
     * @param mappingClass Class名
     * @return
     */
    <E> Page<E> findPageByNativeQuery(final String resourceKey, Map<String, Object> inParams, Pageable pageable, Class<E> mappingClass);

    /**
     * 根据查询语句Key和查询参数返回clazz对象的List。
     *
     * @param resourceKey          指定的SQL
     * @param inParams     SQL参数
     * @param pageable  分页信息
     * @return 分页数据
     */
    Page<Map<String, Object>> findPageByNativeQuery(final String resourceKey, Map<String, Object> inParams, Pageable pageable);

    /**
     * 执行查询,返回单个实体类.
     * <p/>
     * 本函数只能自动处理简单的hql语句,复杂的hql查询请另行编写count语句查询.
     *
     * @param resourceKey 查询SQL Key
     * @param mappingClass 返回的DTO类
     * @param inParams 查询参数
     * @return 结果实体类
     */
    <E> E findOneEntityByNativeQuery(final String resourceKey, Map<String, Object> inParams, Class<E> mappingClass);

    /**
     * 执行查询,返回单个对象.
     * <p/>
     * 本函数只能自动处理简单的hql语句,复杂的hql查询请另行编写count语句查询.
     *
     * @param resourceKey 查询SQL Key
     * @param inParams 查询参数
     * @return 结果对象
     */
    Object findOneObjectByNativeQuery(final String resourceKey, Map<String, Object> inParams);

    /**
     * 根据页面参数返回查询的结果List。
     *
     * @param resourceKey         指定的SQL语句ID
     * @param inParams 分页信息
     * @return 分页查询的结果List
     */
    List<?> findObjectsByNativeQuery(final String resourceKey, final Map<String, Object> inParams);

    /**
     * 根据页面参数返回分页查询的结果List。
     *
     * @param resourceKey         指定的SQL语句ID
     * @param inParams    SQL参数
     * @param pageable 分页信息
     * @return 分页查询的结果List
     */
    Page<?> findPageObjectsByNativeQuery(final String resourceKey, final Map<String, Object> inParams, final Pageable pageable);

    /**
     * 执行查询,返回Map.
     * <p/>
     * 本函数只能自动处理简单的sql语句,复杂的sql查询请另行编写count语句查询.
     *
     * @param resourceKey 查询SQL Key
     * @param inParams 查询参数
     * @return 结果Map
     */
    Map<String, Object> findOneMapByNativeQuery(String resourceKey, Map<String, Object> inParams);

    /**
     * 执行count查询获得本次Sql查询所能获得的对象总数.
     * <p/>
     * 本函数只能自动处理简单的hql语句,复杂的hql查询请另行编写count语句查询.
     *
     * @param resourceKey 查询SQL Key
     * @param inParams 查询参数
     * @return 结果数
     */
    long countNativeQuery(final String resourceKey, final Map<String, Object> inParams);

    /**
     * 根据页面参数返回查询的结果List。
     *
     * @param resourceKey         指定的SQL语句ID
     * @param inParams 分页信息
     * @return 分页查询的结果List
     */
    List<T> findAllByHql(final String resourceKey, final Map<String, Object> inParams);

    /**
     * 根据页面参数返回分页查询的结果List。
     *
     * @param resourceKey         指定的SQL语句ID
     * @param inParams    SQL参数
     * @param pageable 分页信息
     * @return 分页查询的结果List
     */
    Page<T> findPageByHql(final String resourceKey, final Map<String, Object> inParams, final Pageable pageable);


    /**
     * 根据页面参数返回查询的结果List。
     *
     * @param resourceKey         指定的SQL语句ID
     * @param inParams 分页信息
     * @return 分页查询的结果List
     */
    List<?> findObjectsByHql(final String resourceKey, final Map<String, Object> inParams);

    /**
     * 根据页面参数返回分页查询的结果List。
     *
     * @param resourceKey         指定的SQL语句ID
     * @param inParams    SQL参数
     * @param pageable 分页信息
     * @return 分页查询的结果List
     */
    Page<?> findPageObjectsByHql(final String resourceKey, final Map<String, Object> inParams, final Pageable pageable);

    /**
     * 根据查询参数返回分页查询的结果对象。
     *
     * @param resourceKey         指定的SQL语句ID
     * @param inParams 分页信息
     * @return 分页查询的结果List
     */
    T findOneByHql(final String resourceKey, final Map<String, Object> inParams);

    /**
     * 刷新数据，直接从DB刷新
     * @param entity
     */
    void refresh(T entity);
}
