package cn.wewin.modules.springdata.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

/**
 * Find by customer query.
 *
     Usage:
     @NativeQueries (
             queries = {
     @NativeQuery(name="query1",
     sql="SELECT cat.id, cat.nm_chn, cat.nm_eng  FROM CATS c, CATS m WHERE c.MOTHER_ID = c.ID and c.nm_chn =  :catName "),
     @NativeQuery(name="query2",
     sql="SELECT * FROM CATS where c.ID =  :id")
     }
     )
     public interface CatRepository extends MyJpaRepository<Cat, Long> {}

     OR

     @CustomerQueryResources (resourcesName="com.wewin.dao.UserDao.properties")
     public interface CatRepository extends MyJpaRepository<Cat, Long> {}
 *
 * JPA Repository 接口
 *
 * @param <T>
 * @param <ID>
 * @author Jession
 */
@NoRepositoryBean
public interface MyJpaRepository<T, ID extends Serializable> extends JpaRepository<T, ID>, CustomerDynamicQueryJpa<T> {

}
