package cn.wewin.modules.springdata.jpa;

import cn.wewin.modules.springdata.jpa.annotation.CustomQueries;
import cn.wewin.modules.springdata.jpa.annotation.CustomQuery;
import cn.wewin.modules.springdata.jpa.hibernate.JpaHqlQueries;
import cn.wewin.modules.springdata.jpa.hibernate.JpaNativeQueries;
import org.hibernate.Session;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;

/**
 * Simple JPA Repository
 *
 * @param <T>
 * @param <ID>
 * @author Jession
 */
@Repository
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class MySimpleJpaRepository<T, ID extends Serializable> extends
        SimpleJpaRepository<T, ID> implements MyJpaRepository<T, ID> {

    private JpaEntityInformation<T, ?> entityInformation;
    private EntityManager em;
    private Class<?> springDataRepositoryInterface;

    public MySimpleJpaRepository(JpaEntityInformation<T, ?> entityInformation,
                                 EntityManager entityManager, Class<?> springDataRepositoryInterface) {
        super(entityInformation, entityManager);

        this.entityInformation = entityInformation;
        this.em = entityManager;
        this.springDataRepositoryInterface = springDataRepositoryInterface;
    }

    public MySimpleJpaRepository(Class<T> domainClass, EntityManager em, Class<?> springDataRepositoryInterface) {
        this(JpaEntityInformationSupport.getEntityInformation(domainClass, em), em, springDataRepositoryInterface);
    }

    private CustomerQueryInformation getCustomerQueryInformation(String nativeQueryName) {

        Annotation nativeQueryAnn = springDataRepositoryInterface.getAnnotation(CustomQueries.class);
        Session hibernateSession = em.unwrap(Session.class);
        String queryStatement = null;
        String countQueryStatement = null;

        CustomQueries customQueries = (CustomQueries) nativeQueryAnn;
        CustomQuery[] queries = customQueries.queries();

        for (CustomQuery sqlQuery : queries) {
            if (nativeQueryName.equalsIgnoreCase(sqlQuery.name().trim())) {
                queryStatement = sqlQuery.sql();
                countQueryStatement = sqlQuery.countSql();
                break;
            }
        }

        if (queryStatement == null && countQueryStatement == null) {
            return null;
        } else {
            return new CustomerQueryInformation(hibernateSession, queryStatement, countQueryStatement);
        }

    }

    @Override
    public long countHqlResult(String resourceKey, Map<String, Object> inParams) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaHqlQueries<T, ID>(queryInfo).countHqlResult(inParams);
    }

    @Override
    public <E> List<E> findAllByNativeQuery(String resourceKey, Map<String, Object> inParams, Class<E> mappingClass) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaNativeQueries<>(queryInfo, mappingClass).findAll(inParams);
    }

    @Override
    public List<Map<String, Object>> findAllByNativeQuery(String resourceKey, Map<String, Object> inParams) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaNativeQueries<>(queryInfo, Map.class).findAllResultMap(inParams);
    }

    @Override
    public <E> Page<E> findPageByNativeQuery(String resourceKey, Map<String, Object> inParams, Pageable pageable, Class<E> mappingClass) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaNativeQueries<>(queryInfo, mappingClass).findPage(inParams, pageable);
    }

    @Override
    public Page<Map<String, Object>> findPageByNativeQuery(String resourceKey, Map<String, Object> inParams, Pageable pageable) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaNativeQueries<>(queryInfo, Map.class).findPageResultMap(inParams, pageable);
    }

    @Override
    public <E> E findOneEntityByNativeQuery(String resourceKey, Map<String, Object> inParams, Class<E> mappingClass) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaNativeQueries<>(queryInfo, mappingClass).findOneEntity(inParams);
    }

    @Override
    public Object findOneObjectByNativeQuery(String resourceKey, Map<String, Object> inParams) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaNativeQueries<>(queryInfo).findOneObject(inParams);
    }

    @Override
    public List<?> findObjectsByNativeQuery(String resourceKey, Map<String, Object> inParams) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaNativeQueries<>(queryInfo).findObjects(inParams);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Page<?> findPageObjectsByNativeQuery(String resourceKey, Map<String, Object> inParams, Pageable pageable) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaNativeQueries(queryInfo).findObjects(inParams, pageable);
    }

    @Override
    public Map findOneMapByNativeQuery(String resourceKey, Map<String, Object> inParams) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaNativeQueries<>(queryInfo, Map.class).findOneMap(inParams);
    }

    @Override
    public long countNativeQuery(String resourceKey, Map<String, Object> inParams) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaNativeQueries(queryInfo).countNativeQuery(inParams);
    }

    @Override
    public List<T> findAllByHql(String resourceKey, Map<String, Object> inParams) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaHqlQueries<T, ID>(queryInfo).findAll(inParams);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Page<T> findPageByHql(String resourceKey, Map<String, Object> inParams, Pageable pageable) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaHqlQueries(queryInfo).findPage(inParams, pageable);
    }

    @Override
    public List<?> findObjectsByHql(String resourceKey, Map<String, Object> inParams) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaHqlQueries<T, ID>(queryInfo).findObjects(inParams);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Page<?> findPageObjectsByHql(String resourceKey, Map<String, Object> inParams, Pageable pageable) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return queryInfo == null ? null : new JpaHqlQueries(queryInfo).findObjects(inParams, pageable);
    }

    @Override
    public T findOneByHql(String resourceKey, Map<String, Object> inParams) {
        CustomerQueryInformation queryInfo = this.getCustomerQueryInformation(resourceKey);
        return new JpaHqlQueries<T, ID>(queryInfo).findOneEntity(inParams);
    }

    @Override
    public void refresh(T entity) {
        em.refresh(entity);
    }


}
