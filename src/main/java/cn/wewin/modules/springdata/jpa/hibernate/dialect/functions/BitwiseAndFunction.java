package cn.wewin.modules.springdata.jpa.hibernate.dialect.functions;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.Type;

import java.util.List;

/**
 *
 * @author jession
 * @date 7/25/16
 */
public class BitwiseAndFunction extends StandardSQLFunction implements SQLFunction {

    public static final int PARAMETER_SHOULD_NUMBER = 2;

    public BitwiseAndFunction(String name) {
        super(name);
    }

    public BitwiseAndFunction(String name, Type type) {
        super(name, type);
    }

    public String render(List args, SessionFactoryImplementor factory)
            throws QueryException {
        if (args.size() != PARAMETER_SHOULD_NUMBER) {
            throw new IllegalArgumentException(
                    "the function must be passed 2 arguments");
        }
        StringBuffer buffer = new StringBuffer(args.get(0).toString());
        buffer.append(" & ").append(args.get(1));
        return buffer.toString();
    }
}
