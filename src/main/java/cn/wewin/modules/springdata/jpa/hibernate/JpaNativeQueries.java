/**
 *  概要  ：上海巍运信息科技有限公司Framework
 *  作者  ：纪三军
 *  备注  ：作成。
 *
 *  历史：
 *  日期  　　　      担当者名　　　　　　　　　　备注
 *  -----------------------------------------------------
 *  2008-9-10    纪三军          　         作成
 *
 *
 *  ALL RIGHTS RESERVED,COPYRIGHT(C) WE-WIN LIMITED 2007
 */
package cn.wewin.modules.springdata.jpa.hibernate;

import cn.wewin.modules.springdata.jpa.CustomerQueryInformation;
import cn.wewin.modules.springdata.jpa.helper.CustomAliasToBeanResultTransformer;
import cn.wewin.modules.springdata.jpa.helper.SqlBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.query.internal.NativeQueryImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Jession
 */
public final class JpaNativeQueries<T> {

    private Session session;
    private CustomerQueryInformation queryInformation;
    private Class<T> entityClass;

    public JpaNativeQueries(CustomerQueryInformation queryInformation) {
        this.session = queryInformation.getSession();
        this.queryInformation = queryInformation;
    }

    public JpaNativeQueries(CustomerQueryInformation queryInformation, Class<T> mappingClass) {
        this.session = queryInformation.getSession();
        this.queryInformation = queryInformation;
        this.entityClass =  mappingClass;
    }

    /**
     * 根据查询语句Key和查询参数返回clazz对象的List。
     *
     * @param inParams SQL参数
     * @return Result List
     */
    public List<T> findAll(final Map<String, Object> inParams) {
        return findPage(inParams, null).getContent();
    }

    /**
     * 根据查询语句Key和查询参数返回clazz对象的List。
     *
     * @param inParams SQL参数
     * @return Result List
     */
    public List<Map<String, Object>> findAllResultMap(final Map<String, Object> inParams) {
        return findPageResultMap(inParams, null).getContent();
    }

    /**
     * 根据查询语句Key和查询参数返回clazz对象的List。
     *
     * @param inParams SQL参数
     * @param pageable 分页信息
     * @return Page
     */
    @SuppressWarnings("unchecked")
    public Page<T> findPage(final Map<String, Object> inParams, final Pageable pageable) {
        Query<T> query = createNativeSqlQueryResultEntity(queryInformation.getQuery(), inParams);
NativeQuery nativeQuery = (NativeQuery)query;
        return pageable == null ? new PageImpl(nativeQuery.list()) : readPage(query, inParams, pageable);
    }

    /**
     * 根据查询语句Key和查询参数返回clazz对象的List。
     *
     * @param inParams SQL参数
     * @param pageable 分页信息
     * @return Page
     */
    @SuppressWarnings("unchecked")
    public Page<Map<String, Object>> findPageResultMap(final Map<String, Object> inParams, final Pageable pageable) {
        Query<Map<String, Object>> query = createNativeSqlQueryResultMap(queryInformation.getQuery(), inParams);
        query.unwrap(NativeQueryImpl.class).setResultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP);

        return pageable == null ? new PageImpl(query.list()) : readPageResultMap(query, inParams, pageable);
    }

    /**
     * 按SQL查询唯一对象.
     *
     * @param values 命名参数,按名称绑定.
     */
    private T findOneEntity(final String queryString, final Map<String, Object> values) {
        Assert.notNull(entityClass, "entityClass不能为空");
        return createNativeSqlQueryResultEntity(queryString, values)
                .uniqueResult();
    }

    /**
     * 按SQL查询唯一对象.
     *
     * @param values 命名参数,按名称绑定.
     */
    public T findOneEntity(final Map<String, Object> values) {
        return findOneEntity(queryInformation.getQuery(), values);
    }

    /**
     * 按SQL查询唯一对象.
     *
     * @param values 命名参数,按名称绑定.
     */
    private Object findOneObject(final String queryString, final Map<String, Object> values) {
        return createNativeSqlQueryResultObject(queryString, values).uniqueResult();
    }

    /**
     * 按SQL查询唯一对象.
     *
     * @param values 命名参数,按名称绑定.
     */
    public Map<String, Object> findOneMap(final Map<String, Object> values) {
        return createNativeSqlQueryResultMap(queryInformation.getQuery(), values)
                .uniqueResult();
    }

    /**
     * 按SQL查询唯一对象.
     *
     * @param values 命名参数,按名称绑定.
     */
    public Object findOneObject(final Map<String, Object> values) {
        return findOneObject(queryInformation.getQuery(), values);
    }


    /**
     * 根据页面参数返回分页查询的结果List。
     *
     * @param inParams 分页信息
     * @return 分页查询的结果List
     */
    public List<?> findObjects(final Map<String, Object> inParams) {
        return findObjects(inParams, null).getContent();
    }

    /**
     * 根据页面参数返回分页查询的结果List。
     *
     * @param inParams    SQL参数
     * @param pageable 分页信息
     * @return 分页查询的结果List
     */
    @SuppressWarnings("unchecked")
    public Page<?> findObjects(final Map<String, Object> inParams, final Pageable pageable) {
        Query query = createNativeSqlQueryResultObject(queryInformation.getQuery(), inParams);
        return pageable == null ? new PageImpl(query.list()) : readPage(query, inParams, pageable);
    }

    /**
     * Reads the given {@link javax.persistence.TypedQuery} into a {@link org.springframework.data.domain.Page} applying the given {@link org.springframework.data.domain.Pageable} and
     * {@link org.springframework.data.jpa.domain.Specification}.
     *
     * @param query            must not be {@literal null}.
     * @param inParamsForTotal can be {@literal null}.
     * @param pageable         can be {@literal null}.
     * @return Page
     */
    @SuppressWarnings("unchecked")
    private Page<T> readPage(Query<T> query, Map<String, Object> inParamsForTotal, Pageable pageable) {

        query.setFirstResult((int)pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        Long total = countNativeQuery(inParamsForTotal);
        List<T> content = total > pageable.getOffset() ? query.list() : Collections.emptyList();

        return new PageImpl(content, pageable, total);
    }

    /**
     * Reads the given {@link javax.persistence.TypedQuery} into a {@link org.springframework.data.domain.Page} applying the given {@link org.springframework.data.domain.Pageable} and
     * {@link org.springframework.data.jpa.domain.Specification}.
     *
     * @param query            must not be {@literal null}.
     * @param inParamsForTotal can be {@literal null}.
     * @param pageable         can be {@literal null}.
     * @return Page
     */
    @SuppressWarnings("unchecked")
    private Page<Map<String, Object>> readPageResultMap(Query<Map<String, Object>> query, Map<String, Object> inParamsForTotal, Pageable pageable) {
        query.setFirstResult((int) pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        Long total = countNativeQuery(inParamsForTotal);
        List<Map<String, Object>> content = total > pageable.getOffset() ? query.list() : Collections.emptyList();

        return new PageImpl(content, pageable, total);
    }

    /**
     * 执行count查询获得本次Sql查询所能获得的对象总数.
     * <p/>
     * 本函数只能自动处理简单的hql语句,复杂的hql查询请另行编写count语句查询.
     */
    public long countNativeQuery(final Map<String, Object> values) {

        String countQuery = queryInformation.getCountQuery();
        String query = queryInformation.getQuery();
        boolean isCountQuery = StringUtils.isNotBlank(countQuery);

        String finalSql = isCountQuery ? countQuery : prepareCountNativeSql(query);

        try {
            Number count = (Number)findOneObject(finalSql, values);
            return count.longValue();
        } catch (Exception e) {
            throw new RuntimeException("sql can't be auto count, sql is:" + finalSql, e);
        }
    }

    private String prepareCountNativeSql(final String orgSql) {
        String fromSql = orgSql;
        fromSql = StringUtils.substringBeforeLast(fromSql, "order by");

        return "select count(*) from (" + fromSql + ") tmp_count_t";
    }

    /**
     * 根据查询HQL与参数列表创建Query对象.
     * 与find()函数可进行更加灵活的操作.
     *
     * @param values 命名参数,按名称绑定.
     */
    @SuppressWarnings("unchecked")
    private Query<?> createNativeSqlQueryResultObject(final String queryString, final Map<String, Object> values) {
        Query query = createNativeQuery(queryString, values, null);
        return query;
    }

    /**
     * 根据查询HQL与参数列表创建Query对象.
     * 与find()函数可进行更加灵活的操作.
     *
     * @param values 命名参数,按名称绑定.
     */
    private Query<T> createNativeSqlQueryResultEntity(final String queryString, final Map<String, Object> values) {
        Query<T> query = createNativeQuery(queryString, values, entityClass);
        query.unwrap(NativeQueryImpl.class).setResultTransformer(new CustomAliasToBeanResultTransformer(entityClass));

        return query;
    }

    /**
     * 根据查询HQL与参数列表创建Query对象.
     * 与find()函数可进行更加灵活的操作.
     *
     * @param values 命名参数,按名称绑定.
     */
    private Query<Map<String, Object>> createNativeSqlQueryResultMap(final String queryString, final Map<String, Object> values) {

        Assert.hasText(queryString, "queryString不能为空");

        NativeQuery<Map<String, Object>> query = session.createNativeQuery(SqlBuilder.parse(queryString, values));

        if (values != null) {
            query.setProperties(values);
        }

        query.unwrap(NativeQueryImpl.class).setResultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP);

        return query;
    }

    private Query<T> createNativeQuery(String queryString, Map<String, Object> values, Class clazz) {
        Assert.hasText(queryString, "queryString不能为空");

        NativeQuery<T> query;
//        if (clazz != null) {
//            query = session.createNativeQuery(SqlBuilder.parse(queryString, values), clazz);
//        } else {
            query = session.createNativeQuery(SqlBuilder.parse(queryString, values));
//        }
        if (values != null) {
            query.setProperties(values);
        }

        return query;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(-987654321, 37, this);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return <code>true</code> if this object is the same as the obj argument;
     *         <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * Retrieves a text representation for this object.
     *
     * @return the text representation
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
