package cn.wewin.modules.springdata.jpa.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created with IntelliJ IDEA.
 * User: Jession
 * Date: 13-5-23
 * Time: 下午2:26
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomQuery {
    String name() default "";
    String sql() default "";
    String countSql() default "";
}
