package cn.wewin.modules.springdata.jpa.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Jession
 * Date: 4/12/11
 * Time: 2:06 PM
 * @author jession
 */
public class SqlBuilder {

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(SqlBuilder.class);

    /**
     * @param sql
     * @param params
     * @return
     */
    public static String parse(String sql, Map<String, ?> params) {
        if (params == null) {
            return sql;
        }

        String statement = TextTemplates.replaceTemplate(sql, params);
        statement = TextTemplates.removeOptionalFrag(statement, params);
        if (log.isDebugEnabled()) {
            log.debug("Return=" + statement);
        }
        return statement;
    }
}
