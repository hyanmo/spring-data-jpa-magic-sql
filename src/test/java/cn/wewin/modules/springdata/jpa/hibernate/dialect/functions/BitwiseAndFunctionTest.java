package cn.wewin.modules.springdata.jpa.hibernate.dialect.functions;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class BitwiseAndFunctionTest {

    BitwiseAndFunction bitwiseAndFunction;

    @Before
    public void setUp() {
        bitwiseAndFunction = new BitwiseAndFunction("BitwiseAnd");
    }

    @Test
    public void testRender() {
        String result = bitwiseAndFunction.render(Arrays.asList("1", "2"), null);
        Assert.assertEquals("1 & 2", result);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme