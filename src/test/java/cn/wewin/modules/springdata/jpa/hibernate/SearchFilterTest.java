package cn.wewin.modules.springdata.jpa.hibernate;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchFilterTest {

    private Map<String, Object> inParams = new HashMap<>(0);
    private List<SearchFilter> searchFilterMap = new ArrayList<>(0);
    private Map<String, Object> outParams = new HashMap<>(0);

    @Before
    public void setUp() {
        inParams.put("LIKE_userName", "userNameLike");
        inParams.put("EQ_orderNo", "NO123456");

        outParams.put("userName", "%" + "userNameLike" + "%");
        outParams.put("orderNo", "NO123456");

        searchFilterMap.add(new SearchFilter("userName", SearchFilter.Operator.LIKE, "userNameLike"));
        searchFilterMap.add(new SearchFilter("orderNo", SearchFilter.Operator.EQ, "NO123456"));
    }

    @Test
    public void testParseMap() {
        SearchFilter[] in = new SearchFilter[inParams.size()];
        SearchFilter[] out = new SearchFilter[inParams.size()];
        List<SearchFilter> result = SearchFilter.parseMap(inParams);
        Assert.assertArrayEquals(searchFilterMap.toArray(in), result.toArray(out));
    }

    @Test
    public void testToQueryMap() {
        Map<String, Object> result = SearchFilter.toQueryMap(inParams);
        Assert.assertEquals(outParams, result);
    }

    @Test
    public void testGetKey() {
        SearchFilter searchFilter = new SearchFilter("userName", SearchFilter.Operator.LIKE, "userNameLike");
        String result = searchFilter.getKey();
        Assert.assertEquals(SearchFilter.Operator.LIKE + "_" + "userName", result);
    }

    @Test
    public void testGetValue() {
        SearchFilter searchFilter = new SearchFilter("userName", SearchFilter.Operator.LIKE, "userNameLike");
        Object result = searchFilter.getValue();
        Assert.assertEquals("userNameLike", result);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme