/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing rolePerms and
 * limitations under the License.
 */

package cn.wewin.test.springdatajpa.service.example;

import cn.wewin.modules.springdata.jpa.MyJpaRepository;
import cn.wewin.modules.springdata.jpa.annotation.CustomQueries;
import cn.wewin.modules.springdata.jpa.annotation.CustomQuery;
import cn.wewin.test.springdatajpa.domain.example.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@CustomQueries(
		queries = {
				@CustomQuery(
						name = "CQ_City_FindByParams",
						sql = "select * from city " +
								" where 1=1 " +
								"[ and country like :country ]"
				),
				@CustomQuery(
						name = "CQ_City_FindByHqlId",
						sql = "from City " +
								" where id = :id "
				),
				@CustomQuery(
						name = "CQ_City_FindObjectByHql",
						sql = "select name from City "
				),
				@CustomQuery(
						name = "CQ_City_FindById",
						sql = "select * from city " +
								" where id = :id "
				),
				@CustomQuery(
						name = "CQ_City_FindObjectById",
						sql = "select name from city " +
								" where id = :id "
				),
				@CustomQuery(
						name = "CQ_City_FindByHqlParams",
						sql = "from City " +
								" where 1=1 " +
								"[ and name like :name ]"
				),
				@CustomQuery(
						name = "CQ_City_FindIdByParams",
						sql = "select id from city"
				)
		}
)
public interface CityRepository extends MyJpaRepository<City, Long> {

	Page<City> findAll(Pageable pageable);

	Page<City> findByCityNameContainingAndCountryContainingAllIgnoringCase(String name,
			String country, Pageable pageable);

	City findByCityNameAndCountryAllIgnoringCase(String name, String country);

}
