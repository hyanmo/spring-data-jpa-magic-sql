package cn.wewin.test.springdatajpa.service;

import cn.wewin.test.springdatajpa.domain.example.*;
import cn.wewin.test.springdatajpa.service.example.CityRepository;
import cn.wewin.test.springdatajpa.service.example.HotelRepository;
import cn.wewin.test.springdatajpa.service.example.ReviewRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link HotelRepository}.
 *
 * @author Jession
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ReviewRepositoryIntegrationTests {

	@Autowired
	ReviewRepository reviewRepository;

	@Test
	public void findPageByNativeQueryParams() {
		Map<String, Object> params  = new HashMap<>(1);
		params.put("country", "%A%");
		Page<Review> cities = reviewRepository.findPageByNativeQuery("CQ_Review_FindByParams", params, PageRequest.of(1,1), Review.class);
		assertThat(cities.getTotalElements()).isGreaterThan(1);
	}

}
