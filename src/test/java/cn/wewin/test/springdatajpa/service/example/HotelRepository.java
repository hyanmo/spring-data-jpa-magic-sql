/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing rolePerms and
 * limitations under the License.
 */

package cn.wewin.test.springdatajpa.service.example;

import cn.wewin.modules.springdata.jpa.MyJpaRepository;
import cn.wewin.modules.springdata.jpa.annotation.CustomQueries;
import cn.wewin.modules.springdata.jpa.annotation.CustomQuery;
import cn.wewin.test.springdatajpa.domain.example.City;
import cn.wewin.test.springdatajpa.domain.example.Hotel;
import cn.wewin.test.springdatajpa.domain.example.HotelSummary;
import cn.wewin.test.springdatajpa.domain.example.RatingCount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

@CustomQueries(
		queries = {
				@CustomQuery(
						name = "CQ_Hotel_FindByParams",
						sql = "select * from hotel "
				)
		}
)
public interface HotelRepository extends MyJpaRepository<Hotel, Long> {

	Hotel findByCityAndName(City city, String name);

	@Query("select h.city as city, h.name as name, avg(r.rating) as averageRating "
			+ "from Hotel h left outer join h.reviews r where h.city = ?1 group by h")
	Page<HotelSummary> findByCity(City city, Pageable pageable);

	@Query("select r.rating as rating, count(r) as count "
			+ "from Review r where r.hotel = ?1 group by r.rating order by r.rating DESC")
	List<RatingCount> findRatingCounts(Hotel hotel);

}
