package cn.wewin.test.springdatajpa.service;

import cn.wewin.test.springdatajpa.domain.example.*;
import cn.wewin.test.springdatajpa.service.example.CityRepository;
import cn.wewin.test.springdatajpa.service.example.HotelRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link HotelRepository}.
 *
 * @author Jession
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class HotelRepositoryIntegrationTests {

	@Autowired
	CityRepository cityRepository;

	@Autowired
	HotelRepository repository;

	@Test
	public void executesQueryMethodsCorrectly() {
		City city = this.cityRepository
				.findAll(PageRequest.of(0, 1, Direction.ASC, "name")).getContent().get(0);
		assertThat(city.getCityName()).isEqualTo("Atlanta");
		Page<HotelSummary> hotels = this.repository.findByCity(city,
				PageRequest.of(0, 10, Direction.ASC, "name"));
		Hotel hotel = this.repository.findByCityAndName(city,
				hotels.getContent().get(0).getName());
		assertThat(hotel.getName()).isEqualTo("Doubletree");
		List<RatingCount> counts = this.repository.findRatingCounts(hotel);
		assertThat(counts).hasSize(1);
		assertThat(counts.get(0).getRating()).isEqualTo(Rating.AVERAGE);
		assertThat(counts.get(0).getCount()).isGreaterThan(1L);
	}

	@Test
	public void findPageByNativeQueryParams() {
		Map<String, Object> params  = new HashMap<>(1);
		params.put("country", "%A%");
		Page<Hotel> cities = repository.findPageByNativeQuery("CQ_Hotel_FindByParams", params, PageRequest.of(1,1), Hotel.class);
		assertThat(cities.getTotalElements()).isGreaterThan(1);
	}

}
