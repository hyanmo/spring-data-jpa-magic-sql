package cn.wewin.test.springdatajpa.service;

import cn.wewin.test.springdatajpa.SampleDataJpaApplication;
import cn.wewin.test.springdatajpa.domain.example.City;
import cn.wewin.test.springdatajpa.domain.example.CityVO;
import cn.wewin.test.springdatajpa.service.example.CityRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link CityRepository}.
 *
 * @author Jession
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SampleDataJpaApplication.class})
public class CityRepositoryIntegrationTests {

	@Autowired
	private CityRepository repository;

	@Test
	public void findsFirstPageOfCities() {
		Page<City> cities = this.repository.findAll(PageRequest.of(0, 10));
		assertThat(cities.getTotalElements()).isGreaterThan(20L);
	}

	@Test
	public void findAllByNativeQueryParams() {
		Map<String, Object> params  = new HashMap<>(1);
		params.put("country", "%A%");
		List<City> cities = repository.findAllByNativeQuery("CQ_City_FindByParams", params, City.class);
		assertThat(cities.size()).isGreaterThan(1);
	}

	@Test
	public void findAllByNativeQueryOptionalParams() {
		Map<String, Object> params  = new HashMap<>(1);
		List<CityVO> cities = repository.findAllByNativeQuery("CQ_City_FindByParams", params, CityVO.class);
		assertThat(cities.size()).isGreaterThan(20);
	}

	@Test
	public void findPageByNativeQueryParams() {
		Map<String, Object> params  = new HashMap<>(1);
		params.put("country", "%A%");
		Page<City> cities = repository.findPageByNativeQuery("CQ_City_FindByParams", params, PageRequest.of(1,1), City.class);
		assertThat(cities.getTotalElements()).isGreaterThan(1);
	}

	@Test
	public void findPageByNativeQueryOptionalParams() {
		Map<String, Object> params  = new HashMap<>(1);
		Page<CityVO> cities = repository.findPageByNativeQuery("CQ_City_FindByParams", params, PageRequest.of(1,1), CityVO.class);
		assertThat(cities.getTotalElements()).isGreaterThan(20);
	}

	@Test
	public void findObjectsByNativeQueryOptionalParams() {
		Map<String, Object> params  = new HashMap<>(1);
		List<?> cities = repository.findObjectsByNativeQuery("CQ_City_FindIdByParams", params);
		assertThat(cities.size()).isGreaterThan(20);
	}

	@Test
	public void findPageObjectsByNativeQueryOptionalParams() {
		Map<String, Object> params  = new HashMap<>(1);
		Page<?> cities = repository.findPageObjectsByNativeQuery("CQ_City_FindByParams", params, PageRequest.of(1,1));
		assertThat(cities.getTotalElements()).isGreaterThan(20);
	}

	@Test
	public void findAllByHqlQueryParams() {
		Map<String, Object> params  = new HashMap<>(1);
		params.put("name", "%A%");
		List<City> cities = repository.findAllByHql("CQ_City_FindByHqlParams", params);
		assertThat(cities.size()).isGreaterThan(1);
	}

	@Test
	public void findAllByHqlQueryOptionalParams() {
		Map<String, Object> params  = new HashMap<>(1);
		List<City> cities = repository.findAllByHql("CQ_City_FindByHqlParams", params);
		assertThat(cities.size()).isGreaterThan(20);
	}

	@Test
	public void findPageByHqlQueryParams() {
		Map<String, Object> params  = new HashMap<>(1);
		params.put("name", "%A%");
		Page<City> cities = repository.findPageByHql("CQ_City_FindByHqlParams", params, PageRequest.of(1, 1));
		assertThat(cities.getTotalElements()).isGreaterThan(1);
	}

	@Test
	public void findOneEntityByHql() {
		Map<String, Object> params = new HashMap<>(1);
		params.put("id", 1l);
		City city = repository.findOneByHql("CQ_City_FindByHqlId", params);
		assertThat(city).isNotNull();
	}

	@Test
	public void findObjectByHql() {
		Map<String, Object> params = new HashMap<>(1);
		params.put("id", 1l);
		List cities = repository.findObjectsByHql("CQ_City_FindObjectByHql", params);
		assertThat(cities.size()).isEqualTo(21);
	}

	@Test
	public void findPageObjectByHql() {
		Map<String, Object> params = new HashMap<>(1);
		params.put("id", 1l);
		Page<?> cities = repository.findPageObjectsByHql("CQ_City_FindObjectByHql", params, PageRequest.of(1, 1));
		assertThat(cities.toString()).isEqualTo(21);
	}


	@Test
	public void findPageByHqlQueryOptionalParams() {
		Map<String, Object> params  = new HashMap<>(1);
		Page<City> cities = repository.findPageByHql("CQ_City_FindByHqlParams", params, PageRequest.of(1, 1));
		assertThat(cities.getTotalElements()).isGreaterThan(20);
	}

	@Test
	public void findByNameAndCountryAllIgnoringCase() {
		City city = repository.findByCityNameAndCountryAllIgnoringCase("Brisbane", "Australia");
		assertThat(city).isNotNull();
	}

	@Test
	public void testRefresh() {
		City city = repository.findById(1l).get();
		assertThat(city).isNotNull();
	}

	@Test
	public void countHqlResult() {
		long count = repository.countHqlResult("CQ_City_FindByHqlParams", new HashMap<>(0));
		assertThat(count).isEqualTo(21);
	}

	@Test
	public void findAllResultMapByNativeQuery() {
		Map<String, Object> params = new HashMap<>(1);
		List<Map<String, Object>> cities = repository.findAllByNativeQuery("CQ_City_FindByParams", params);
		assertThat(cities.size()).isGreaterThan(20);
	}

	@Test
	public void findPageResultMapByNativeQuery() {
		Map<String, Object> params = new HashMap<>(1);
		params.put("country", "%A%");
		Page<Map<String, Object>> cities = repository.findPageByNativeQuery("CQ_City_FindByParams", params, PageRequest.of(1, 1));
		assertThat(cities.getTotalElements()).isGreaterThan(1);
	}

	@Test
	public void findOneEntityByNativeQuery() {
		Map<String, Object> params = new HashMap<>(1);
		params.put("id", 1l);
		City city = repository.findOneEntityByNativeQuery("CQ_City_FindById", params, City.class);
		assertThat(city).isNotNull();
	}

	@Test
	public void findOneObjectByNativeQuery() {
		Map<String, Object> params = new HashMap<>(1);
		params.put("id", 1l);
		Object city = repository.findOneObjectByNativeQuery("CQ_City_FindObjectById", params);
		assertThat(city).isNotNull();
	}

	@Test
	public void findOneMapByNativeQuery() {
		Map<String, Object> params = new HashMap<>(1);
		params.put("id", 1l);
		Map<String, Object> city = repository.findOneMapByNativeQuery("CQ_City_FindById", params);
		assertThat(city).isNotNull();
	}

	@Test
	public void countNativeQuery() {
		Map<String, Object> params = new HashMap<>(1);
		params.put("country", "%A%");
		long count = repository.countNativeQuery("CQ_City_FindByParams", params);
		assertThat(count).isGreaterThan(1);
	}



}
